﻿#include <iostream>

int* FillArrayFromZero(int* arr,int n1,int n)
{
    for (int j = 0; j < n1; j++)
    {
        for (int i = 0; i < n; i++)
        {
            arr[i] = rand() % 100;
            std::cout << arr[i] << '\t';
        }
    }
    
    std::cout << '\n';
    return arr;
}

void FirstTask()
{
    int n1, n2;
    std::cout << "Type size of A and B array\n";
    std::cin >> n1;
    std::cin >> n2;
    int* A = new int[n1];
    int* B = new int[n2];
    int IndA = -1;
    int IndB = -1;
    FillArrayFromZero(A,1,n1);
    FillArrayFromZero(B,1,n2);
    for (int i = 0; i < n1; i++)
    {
        if (A[i] < 0)
        {
            IndA = i;
            break;
        }
    }
    for (int i = 0; i < n2; i++)
    {
        if (B[i] < 0)
        {
            IndB= i;
            break;
        }
    }
    int DelA = n1 - IndA - 1;
    int SumA = 0;
    for (int i = IndA + 1; i < n1; i++)
    {
        SumA += A[i];
    }
    if (DelA == 0)
    {
        std::cout << "arithmetic mean of array A = " << SumA << '\n';
    }
    else
    {
        std::cout << "arithmetic mean of array A = " << SumA / DelA << '\n';
    }
    


    int DelB = n2 - IndB - 1;
    int SumB = 0;
    for (int i = IndB + 1; i < n2; i++)
    {
        SumB += B[i];
    }
    if (DelB == 0)
    {
        std::cout << "arithmetic mean of array B = " << SumB<< '\n';
    }
    else
    {
        std::cout << "arithmetic mean of array B = " << SumB / DelB << '\n';
    }
}

void SecondTask()
{
    const int n = 18;
    int arr[n][n];
    for (int j = 0; j < n; j++)
    {
        for (int i = 0; i < n; i++)
        {
            arr[j][i] = rand() % 100;
            std::cout << arr[j][i] << '\t';
        }
        std::cout << '\n';
    }

    std::cout << '\n';

    int count = 0;
    int arr2[n] = {0};
    for (int i = 0; i <= int(n / 2) + 1; i++)
    {
        for (int j = i; j <= int((n)/2); j++)
        {
            if ((n-j-1) > j && (i < n-i-1))
            {
                if (arr[i][j] == 0)
                {   
                    count += 1;
                    arr2[i] += 1;
                }
                if (arr[i][n - j - 1] == 0)
                {
                    count += 1;
                    arr2[i] += 1;
                }
                if (arr[n - i - 1][j] == 0)
                {
                    count += 1;
                    arr2[n - i - 1] += 1;
                }
                if (arr[n - i - 1][n - j - 1] == 0)
                {
                    count += 1;
                    arr2[n - i - 1] += 1;
                }
            }
            else if (n - j - 1 == j && (i < n - i - 1))
            {
                if (arr[i][n - j - 1] == 0)
                {
                    count += 1;
                    arr2[i] += 1;
                }
                if (arr[n - i - 1][n - j - 1] == 0)
                {
                    count += 1;
                    arr2[n - i - 1] += 1;
                }
            }
            
        }
    }
    std::cout << count << '\n';
    for (int i = 0; i < n; i++)
    {
        std::cout << "В " << i+1 << " строчке - " << arr2[i] << " нулей'\n";
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");
    //std::cout << "First task started \n";
    //FirstTask();
    //std::cout << "====================================================== \n";
    std::cout << "Second task started \n";
    SecondTask();

}